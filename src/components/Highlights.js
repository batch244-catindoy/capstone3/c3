import { useEffect, useState } from "react";
import { Card, Col, Row } from "react-bootstrap";
import safe from "./picture/safe.jpg" 

export default function Highlights() {
  const [activeProducts, setActiveProducts] = useState();

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/viewActive`)
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          setActiveProducts(
            data.map((product, index) => {
              return (
                <Col>
                  <Card key={index}>
                    <Card.Img src={safe} />
                    <Card.Body>
                      <Card.Title>{product.name}</Card.Title>
                      <Card.Text>{product.description}</Card.Text>
                    </Card.Body>
                  </Card>
                </Col>
              );
            })
          );
        } else {
          setActiveProducts([]);
        }
      });
  });

  return (
    <Row xs={1} md={2} className="g-4">
      {activeProducts}
    </Row>
  );
}